import requests


base_url = 'http://localhost:5000/'
data = requests.post(f'{base_url}keys/private/create/',
                     params=[('length', '4096')])


private_key = data.content.decode()
data = requests.post(f'{base_url}certificates/create',
                     params=[('private', private_key),
                             ('fio', 'Some'),
                             ('department', 'Another'),
                             ('email', 'veryhard@test.com')]
                     )
with open('ClientPrivateKey', 'w') as file:
    file.write(private_key)
