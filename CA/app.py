from flask import Flask, request
import subprocess
from datetime import datetime
import hashlib
import os
from werkzeug.datastructures import FileStorage

app = Flask(__name__)
KEY_DIR = './key/'
TEMP_DIR = './temp/'
REQ_DIR = './req/'
CLIENT_CERTS_DIR = './certs/'
HASH_DIR = './hashes/'
FILES_DIR = './user_files/'
SIGNATURES_DIR = './signatures/'
CERTS_BY_SERIAL_DIR = './newcerts/'


@app.route('/keys/private/create/', methods=['POST'])
def create_private_key():
    """
    Создаёт приватный ключ, не сохраняя на сервере
    :return:
    """
    length = request.args.get('length')
    command = f'openssl genrsa {length}'.split()
    private_key = subprocess.run(command, capture_output=True)

    return private_key.stdout  # ToDo encrypt


def save_private_key_to_temp_file(private_key):
    temp_file_name = f'{hashlib.sha512(str(datetime.now()).encode()).hexdigest()}-private.key'
    with open(TEMP_DIR + temp_file_name, 'w') as private_key_path:
        private_key_path.write(private_key)

    return temp_file_name


@app.route('/keys/public/create/', methods=['POST'])
def generate_public_key():
    """
    Принимает закрытый ключ, создаёт открытый, сохраняя его на сервере
    :return:
    """
    fio = request.args.get('fio')
    department = request.args.get('department')
    private_key = request.args.get('private')

    temp_file_name = save_private_key_to_temp_file(private_key)

    public_file_name = fio + department + str(datetime.now())
    public_file_name = f'{hashlib.sha512(public_file_name.encode()).hexdigest()}.public'
    command = f'openssl rsa -in {TEMP_DIR+temp_file_name} -pubout'.split()
    data = subprocess.run(command, capture_output=True)

    with open(KEY_DIR+public_file_name, 'w') as file:
        file.write(data.stdout.decode())

    os.remove(TEMP_DIR + temp_file_name)

    return public_file_name


def generate_certificate_request(department, fio, email, password, private_key_file):
    config_file_format = f"""
        [ req ]
        default_bits           = 4096
        distinguished_name     = req_distinguished_name
        attributes             = req_attributes
        prompt                 = no

        [ req_distinguished_name ]
        C                      = RU
        ST                     = Perm krai
        L                      = Perm
        O                      = Test
        OU                     = {department}
        CN                     = {fio}
        emailAddress           = {email}

        [ req_attributes ]
        challengePassword              = {password}
        """
    temp_config = f'{fio}{department}{datetime.now()}'.encode()
    temp_config = f'{hashlib.sha512(temp_config).hexdigest()}.conf'
    with open(TEMP_DIR+temp_config, 'w') as config:
        config.write(config_file_format)

    request_file = f'Request from {fio} {department} at {int(datetime.now().timestamp())}.req'.replace(" ", "_")

    request_command = f'openssl req -new -key {TEMP_DIR + private_key_file} -sha512 -out {REQ_DIR + request_file}' \
                      f' -config {TEMP_DIR+temp_config}'.split()
    data = subprocess.run(request_command, capture_output=True)

    return temp_config, request_file


def ensure_crl():
    command = 'openssl ca -config EAI_openssl.conf -gencrl ' \
              '-keyfile private/cakey.pem -cert certs/EAI.crt -out ca.crl.pem'.split()
    subprocess.run(command, capture_output=True)
    command = 'openssl crl -in ca.crl.pem -outform DER -out ca.crl'.split()
    subprocess.run(command, capture_output=True)


def generate_certificate(request_file):
    certificate_filename = f"Certificate_for_{request_file.replace('.req', '.crt')}"
    command = f'openssl ca -in {REQ_DIR+request_file} -config EAI_openssl.conf ' \
              f'-batch -out {CLIENT_CERTS_DIR+certificate_filename}'.split()
    subprocess.run(command, capture_output=True)
    ensure_crl()

    return certificate_filename


def get_certificate_details(cert_filename):
    command = f'openssl x509 -noout -serial -startdate -enddate -in {CLIENT_CERTS_DIR+cert_filename}'.split()
    data = subprocess.run(command, capture_output=True).stdout.decode().splitlines()

    return {
        data[n].split('=')[0]: data[n].split('=')[1] for n in range(len(data))
    }


@app.route('/certificates/create', methods=['POST'])
def create_certificate():
    fio = request.args.get('fio')
    department = request.args.get('department')
    email = request.args.get('email')
    password = request.args.get('password')

    private_key = request.args.get('private')
    temp_file_name = save_private_key_to_temp_file(private_key)
    temp_config, request_file = generate_certificate_request(department, fio, email, password, temp_file_name)

    cert_filename = generate_certificate(request_file)
    cert_details = get_certificate_details(cert_filename)

    os.remove(TEMP_DIR+temp_config)
    os.remove(TEMP_DIR+temp_file_name)
    return {
        'certificate_filename': cert_filename,
        'details': cert_details
    }


def deny_certificate(certificate_name):
    command = f"openssl ca -config EAI_openssl.conf -revoke {CERTS_BY_SERIAL_DIR+certificate_name} " \
              f"-keyfile private/cakey.pem -cert certs/EAI.crt".split()

    subprocess.run(command, capture_output=True)


@app.route('/certificates/revoke', methods=['POST'])
def revoke_cert():
    cert_serial = request.args.get('cert_id')
    cert_name = f'{cert_serial}.pem'

    deny_certificate(cert_name)

    return 'OK', 200


def sign_document(private_key, document):
    private_key_file = save_private_key_to_temp_file(private_key)
    signature_name = hashlib.sha512(f'{document}{private_key}{datetime.now()}'.encode()).hexdigest()
    command = f'openssl dgst -sha512 -sign {TEMP_DIR + private_key_file} ' \
              f'-out {SIGNATURES_DIR + signature_name} {HASH_DIR + document}'.split()
    data = subprocess.run(command, capture_output=True)

    os.remove(TEMP_DIR+private_key_file)

    return signature_name


@app.route('/documents/sign', methods=['POST'])
def sign_doc():
    file_hash = request.args.get('file_hash') or None
    file_hash_name = request.args.get('file_hash_name') or None
    private_key = request.args.get('private_key') or None
    with open(f'{HASH_DIR}{file_hash_name}', 'w') as file:
        file.write(file_hash)

    signature_path = sign_document(private_key, file_hash_name)

    return signature_path


def check_signature(public_key, document, signature):
    command = f'openssl dgst -sha512 -verify {KEY_DIR + public_key}' \
              f' -signature {SIGNATURES_DIR + signature} {document}'.split()

    data = subprocess.run(command, capture_output=True)
    return data.stdout


@app.route('/signatures/check', methods=['POST'])
def check_sign():
    signature = request.args.get('sign_name')
    document_hash = request.args.get('doc_hash')
    public_key = request.args.get('public')
    document = TEMP_DIR + document_hash
    with open(document, 'w') as file:
        file.write(document_hash)

    verified = check_signature(public_key, document, signature)
    os.remove(document)

    return {'verified': verified.decode()}


@app.route('/files/', methods=['POST'])
def save_user_file():
    filename = request.args.get('filename') or 'AAA'
    FileStorage(request.stream).save(f'{FILES_DIR}{filename}')
    return 'OK', 200


if __name__ == '__main__':
    app.run()
