function sendAgreement(agreed) {
    const csrf_token = getCookie('csrftoken');
    const request = new Request('#', {headers: {'X-CSRFToken': csrf_token}});
    fetch(request, {
        method: 'POST',
        body: agreed
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
}