function get_current_cert() {
    const csrfToken = getCookie('csrftoken');
    fetch('#', {
        method: 'POST',
        headers: {'X-CSRFToken': csrfToken},
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
        console.log(response)
    })
}
