let blobParts = "";

function createCertificate() {
    const csrfToken = getCookie('csrftoken');
    fetch('#', {
        method: 'POST',
        headers: {'X-CSRFToken': csrfToken},
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
        // ToDo handle when JSON
        const reader = response.body.getReader()
        reader.read().then(function processData({ done, value }) {
            if (done) {
                const file = new Blob([blobParts], {type: "text/plain"})
                let link = document.createElement("a");
                link.setAttribute("href", URL.createObjectURL(file));
                link.setAttribute("download", "details");
                link.click();
                window.location.href = '/';
            }
            value.forEach((item, i, arr) => {
                blobParts += String.fromCharCode(item)
            })

            return reader.read().then(processData);
        })
    })
}