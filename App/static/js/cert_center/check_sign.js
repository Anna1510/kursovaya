function checkSign(id) {
    const csrfToken = getCookie('csrftoken');

    fetch(`/documents/${id}/check/`, {
        method: 'POST',
        headers: {'X-CSRFToken': csrfToken},
        body: id
    }).then(response => {
        response.json()
            .then((data) => {
                const cell = document.getElementById(`doc_check_${id}`)
                if (data['verified']) {
                    cell.innerHTML = "Подлинность подтверждена"
                }
            })
    })
}