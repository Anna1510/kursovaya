function choose_receiver(id) {
    const csrfToken = getCookie('csrftoken');
    const chosen = JSON.stringify(id)
    fetch('#', {
        method: 'POST',
        headers: {'X-CSRFToken': csrfToken},
        body: chosen
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
}
