function revokeCertificate(id) {
    const csrf_token = getCookie('csrftoken');
    const doc_id = JSON.stringify({'doc id': id});
    const request = new Request('revoke/', {headers: {'X-CSRFToken': csrf_token}});
    fetch(request, {
        method: 'POST',
        body: doc_id
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
    .catch(function(err) {
        console.info(err);
    });
}