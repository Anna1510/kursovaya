function sendDoc(id) {
    const csrf_token = getCookie('csrftoken');
    const doc_id = JSON.stringify({'doc id': id});
    const request = new Request('#', {headers: {'X-CSRFToken': csrf_token}});
    fetch(request, {
        method: 'POST',
        body: doc_id
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
    .catch(function(err) {
        console.info(err);
    });
}

function redirectToSend(id) {
    const csrf_token = getCookie('csrftoken');
    const request = new Request(`/documents/send/${id}`, {headers: {'X-CSRFToken': csrf_token}});
    fetch(request, {
        method: 'GET'
    }).then(response => {
        window.location.href = response.url
    })
}

function redirectToChooseReceiver(id) {
    const csrf_token = getCookie('csrftoken');
    const request = new Request(`/documents/${id}/choose_receiver`, {headers: {'X-CSRFToken': csrf_token}});
    fetch(request, {
        method: 'GET'
    }).then(response => {
        console.log(response)
        window.location.href = response.url
    })
}