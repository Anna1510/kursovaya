function Agreed() {
    let agreed_checkbox = document.getElementById("agreed");
    if (!agreed_checkbox.checked) {
        alert('Прочитайте соглашение и поставье галочку напротив "Я принимаю условия Соглашения"');
    }
    const url = '#';
    const csrfToken = getCookie('csrftoken');
    fetch(url, {
        method: 'POST',
        mode: 'same-origin',
        headers: {'X-CSRFToken': csrfToken},
        body: JSON.stringify({'agreed': agreed_checkbox.checked})
    })
    .then((resp) => {
        if (resp.redirected) {
            window.location.href = resp.url;
        }
    })
    .catch((e) => {
        alert(e.message)
    })
}
