function logout() {
    const csrfToken = getCookie('csrftoken');
    fetch('#', {
        method: 'POST',
        headers: {'X-CSRFToken': csrfToken},
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
}