import dateutil.parser as date_parser
from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import JsonResponse, FileResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.utils import timezone
from .mixins import PasswordChangeRequiredMixin
from django.conf import settings
from django.contrib.auth.models import User
from .models import Certificate, Document, Events
from .forms import DocumentUploadForm, PrivateKeyUploadForm
from .utils import safe_get_document_by_id, WrongDocException, safe_get_certificate_by_id, WrongCertException
import json
import hashlib
import requests
from transliterate import translit
import os

TEMPLATES_SUB_FOLDER = 'cert_center/'
MAIN_PAGE_TEMPLATE = TEMPLATES_SUB_FOLDER + 'index.html'
MY_DOCUMENTS_TEMPLATE = TEMPLATES_SUB_FOLDER + 'my_docs.html'
CHOOSE_RECEIVER_TEMPLATE = TEMPLATES_SUB_FOLDER + 'choose_receiver.html'
DOCUMENT_UPLOAD_TEMPLATE = TEMPLATES_SUB_FOLDER + 'document_upload.html'
DOCUMENT_SEND_TEMPLATE = TEMPLATES_SUB_FOLDER + 'document_send.html'
DOCUMENT_SIGN_TEMPLATE = TEMPLATES_SUB_FOLDER + 'document_sign.html'
DOCUMENT_SIGN_AGREEMENT_TEMPLATE = TEMPLATES_SUB_FOLDER + 'document_sign_agreement.html'
CERTIFICATES_CREATE_TEMPLATE = TEMPLATES_SUB_FOLDER + 'certificates_create.html'
CERTIFICATES_VIEW_TEMPLATE = TEMPLATES_SUB_FOLDER + 'certificates.html'
CERTIFICATE_VIEW_TEMPLATE = TEMPLATES_SUB_FOLDER + 'certificate.html'
EVENTS_TEMPLATE = TEMPLATES_SUB_FOLDER + 'events.html'
EVENT_VIEW_TEMPLATE = TEMPLATES_SUB_FOLDER + 'event.html'


@method_decorator(login_required, name='dispatch')
class CertificationCenterView(PasswordChangeRequiredMixin, View):
    def get(self, request):
        user = request.user
        docs = list(Document.objects.filter(to_who=user))
        return render(request, MAIN_PAGE_TEMPLATE, {'docs': docs, 'MEDIA_URL': settings.MEDIA_URL})


@method_decorator(login_required, name='dispatch')
class MyDocumentsView(PasswordChangeRequiredMixin, View):
    def get(self, request):
        user = request.user
        form = DocumentUploadForm()
        docs = list(Document.objects.filter(from_who=user))
        return render(request, MY_DOCUMENTS_TEMPLATE, {'docs': docs, 'form': form})

    @staticmethod
    def save_user_file(file, user):
        filename = f'{user.last_name} {user.first_name} {user.edcuser.middle_name} {file.name} {timezone.now()}'
        filename = hashlib.sha512(filename.encode()).hexdigest()
        extension = '.' + file.name.split('.')[-1] if len(file.name.split('.')) > 1 else ''
        filename += extension
        file_hash = hashlib.sha512()

        with open(settings.MEDIA_ROOT + filename, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
                file_hash.update(chunk)
        with open(settings.MEDIA_ROOT + filename, 'rb') as file_to_send:
            ca_url = settings.CA_SERVER_URL
            requests.post(f'{ca_url}files/', data=file_to_send, params=[('filename', filename)])

        return filename, file_hash.hexdigest()

    def post(self, request):
        form = DocumentUploadForm(request.POST, request.FILES)

        if form.is_valid():
            user = request.user
            original_filename = request.FILES['file'].name
            filename, file_hash = self.save_user_file(request.FILES['file'], user)
            doc = Document(from_who=user, download_date=timezone.now(), hash=file_hash, path=filename,
                           original_name=original_filename)
            doc.save()

            return redirect(to='/')
        return JsonResponse({'error': 'something wrong with form'})


@method_decorator(login_required, name='dispatch')
class CertificatesCreateView(PasswordChangeRequiredMixin, View):
    def get(self, request):
        return render(request, CERTIFICATES_CREATE_TEMPLATE)

    @staticmethod
    def get_key_pair(url, fio, department):
        private_key = requests.post(f'{url}keys/private/create/', params=[('length', '4096')])
        public_key = requests.post(f'{url}keys/public/create/', params=[
            ('fio', fio),
            ('department', department),
            ('private', private_key.text)
        ])
        return private_key, public_key

    def create_certificate(self, user):
        ca_url = settings.CA_SERVER_URL
        fio = translit(f'{user.last_name} {user.first_name} {user.edcuser.middle_name}', reversed=True).replace("'", '')
        department = translit(user.edcuser.department, reversed=True).replace("'", '')
        email = user.email
        password = user.password.split('$')[-1]

        private_key, public_key = self.get_key_pair(ca_url, fio, department)

        cert = requests.post(f'{ca_url}certificates/create', params=[('private', private_key.text),
                                                                     ('fio', fio),
                                                                     ('department', department),
                                                                     ('email', email),
                                                                     ('password', password)]
                             )
        return cert, private_key, public_key

    @staticmethod
    def save_certificate_and_public_key_data_to_db(cert, public_key, user):
        cert_details = cert.json()['details']
        starting_date = date_parser.parse(cert_details['notBefore'])
        ending_date = date_parser.parse(cert_details['notAfter'])
        serial = int(cert_details['serial'], 16)
        certificate = Certificate(owner=user, starting_date=starting_date, ending_date=ending_date,
                                  serial=serial, status='Действует')

        user.edcuser.public_key = public_key.text
        user.edcuser.save()
        certificate.public_key_filename = public_key.text
        certificate.save()

    def post(self, request):
        user = request.user
        user_working_certs = list(user.certificate_set.filter(status='Действует'))
        if len(user_working_certs) > 1:
            return JsonResponse({'error': 'Too many working certs, contact admin'})
        if len(user_working_certs) == 1:
            return JsonResponse(
                {'error': 'Already has working certificate, deny it and create new or user already existing'}
            )

        cert, private_key, public_key = self.create_certificate(user)

        self.save_certificate_and_public_key_data_to_db(cert, public_key, user)

        json_file = {
            'private_key': private_key.text,
            'public_key': public_key.text,
            'certificate_filename': cert.json()['certificate_filename']
        }

        json_file = json.dumps(json_file)
        return FileResponse(json_file)


@method_decorator(login_required, name='dispatch')
class SendDocumentView(PasswordChangeRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        return render(request, DOCUMENT_SEND_TEMPLATE, {'doc_id': kwargs['doc_id']})

    def post(self, request, *args, **kwargs):
        user = request.user
        data = json.loads(request.body)
        doc_id = data.get('doc id') or None
        try:
            document = safe_get_document_by_id(doc_id, user)
        except WrongDocException as exception:
            return JsonResponse({'error': exception})

        if not document.signature_date:
            return redirect(to=f'/documents/sign/{doc_id}')
        if not document.to_who:
            return redirect(to=f'/documents/{doc_id}/choose_receiver')
        if document.sent_date:
            return redirect(to='/')

        event = Events(subject=user, object=document.to_who, date=timezone.now(), type='DocSent')
        event.details = {'doc_id': doc_id,
                         'seen': False,
                         'text': f'Вам был отправлен документ от {user.last_name} {user.first_name} {user.edcuser.middle_name}',
                         'heading': 'Новый документ'}
        event.save()

        document.sent_date = timezone.now()
        document.received_date = timezone.now()

        document.save()

        return redirect(to='/')


@method_decorator(login_required, name='dispatch')
class ChooseReceiverView(PasswordChangeRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        users = list(User.objects.all())
        media_url = settings.MEDIA_URL
        return render(request, CHOOSE_RECEIVER_TEMPLATE, {'users': users, 'MEDIA_URL': media_url})

    def post(self, request, *args, **kwargs):
        chosen = int(request.body)
        user = request.user
        doc_id = kwargs.get('doc_id') or None
        try:
            document = safe_get_document_by_id(doc_id, user)
        except WrongDocException as exception:
            return JsonResponse({'error': exception})
        to_who = User.objects.get(id=chosen)
        document.to_who = to_who
        document.save()
        return redirect(to='/')


@method_decorator(login_required, name='dispatch')
class SignDocumentView(PasswordChangeRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        doc_id = kwargs.get('doc_id') or None
        user = request.user
        if not user.edcuser.get_working_certificate():
            return redirect(to='/certificates/create/')
        try:
            safe_get_document_by_id(doc_id, user)
        except WrongDocException as exception:
            return JsonResponse({'error': exception})

        return render(request, DOCUMENT_SIGN_TEMPLATE)

    def post(self, request, *args, **kwargs):
        doc_id = kwargs.get('doc_id')
        user = request.user
        if not user.edcuser.get_working_certificate():
            return redirect(to='/certificates/create/')
        try:
            document = safe_get_document_by_id(doc_id, user)
        except WrongDocException as exception:
            return JsonResponse({'error': exception})

        doc_path = settings.MEDIA_ROOT + document.path
        if os.path.exists(doc_path):
            return FileResponse(open(doc_path, 'rb'), as_attachment=True, filename=document.original_name)
        return redirect(to='/')


@method_decorator(login_required, name='dispatch')
class SignDocumentAgreementView(PasswordChangeRequiredMixin, View):
    def get_private_key_from_file(self, file):
        output = ""
        for chunk in file.chunks():
            output += chunk.decode()
        output = json.loads(output)

        return output['private_key']

    def get(self, request, *args, **kwargs):
        doc_id = kwargs.get('doc_id') or None
        user = request.user
        try:
            safe_get_document_by_id(doc_id, user)
        except WrongDocException as exception:
            return JsonResponse({'error': exception})
        form = PrivateKeyUploadForm()
        return render(request, DOCUMENT_SIGN_AGREEMENT_TEMPLATE, {'form': form})

    def post(self, request, *args, **kwargs):
        doc_id = kwargs.get('doc_id') or None
        form = PrivateKeyUploadForm(request.POST, request.FILES)
        user = request.user
        if not user.edcuser.get_working_certificate():
            return redirect(to='/certificates/create/')
        if not form.is_valid():
            form = PrivateKeyUploadForm()
            return render(request, DOCUMENT_SIGN_AGREEMENT_TEMPLATE, {'form': form})

        if form.cleaned_data['agreed'] != 'yes':
            return redirect(to='/')

        try:
            document = safe_get_document_by_id(doc_id, user)
        except WrongDocException:
            return redirect(to='/')

        ca_url = settings.CA_SERVER_URL
        private_key = self.get_private_key_from_file(request.FILES['file'])
        signature_path = requests.post(f'{ca_url}documents/sign', params=[('file_hash', document.hash),
                                                                          ('private_key', private_key),
                                                                          ('file_hash_name', document.path)])
        document.signature_date = timezone.now()
        document.signature_path = signature_path.content.decode()
        certificate = user.edcuser.get_working_certificate()
        document.certificate = certificate
        document.save()

        return redirect(to='/documents/')


@method_decorator(login_required, name='dispatch')
class CertificatesView(PasswordChangeRequiredMixin, View):
    def get(self, request):
        certificates = list(request.user.certificate_set.all())
        return render(request, CERTIFICATES_VIEW_TEMPLATE, {'certs': certificates})

    def post(self, request):
        return redirect(to=f'/certificates/current/')


@method_decorator(login_required, name='dispatch')
class CertificateView(PasswordChangeRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        user = request.user
        cert_id = kwargs.get('cert_id') or None
        if cert_id:
            try:
                certificate = safe_get_certificate_by_id(cert_id, user)
            except WrongCertException as e:
                return JsonResponse({'error': e})
        else:
            certificate = user.edcuser.get_working_certificate()

        return render(request, CERTIFICATE_VIEW_TEMPLATE, {'cert': certificate, 'user': user})


@method_decorator(login_required, name='dispatch')
class RevokeCertificateView(PasswordChangeRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        cert_id = kwargs.get('cert_id') or None
        user = request.user
        try:
            certificate = safe_get_certificate_by_id(cert_id, user)
        except WrongCertException as e:
            return JsonResponse({'error': e})

        if certificate.status != 'Действует':
            return redirect(to='/')

        certificate.status = 'Аннулирован'
        certificate.explanation = 'По требованию владельца'
        certificate.ending_date = timezone.now()

        ca_url = settings.CA_SERVER_URL
        requests.post(f'{ca_url}certificates/revoke', params=[
            ('cert_id', hex(certificate.serial).replace('0x', ''))
        ])

        certificate.save()
        return redirect(to='/')  # ToDo handle reports


@method_decorator(login_required, name='dispatch')
class CheckDocumentView(PasswordChangeRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        doc_id = kwargs.get('doc_id')
        user = request.user

        try:
            document = safe_get_document_by_id(doc_id, user)
        except WrongDocException as e:
            return JsonResponse({'error': e})

        ca_url = settings.CA_SERVER_URL
        file_hash = hashlib.sha512()

        with open(settings.MEDIA_ROOT + document.path, 'rb') as file:
            chunk = file.read(4096)
            while chunk:
                file_hash.update(chunk)
                chunk = file.read(4096)

        data = requests.post(f'{ca_url}signatures/check', params=[
            ('sign_name', document.signature_path),
            ('doc_hash', file_hash.hexdigest()),
            ('public', document.certificate.public_key_filename)
        ])

        verified = data.json()['verified'].split(' ')[-1].strip()
        document.checked = verified == 'OK'
        document.save()

        return JsonResponse({'verified': verified == 'OK'})


@method_decorator(login_required, name='dispatch')
class EventsView(PasswordChangeRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        user = request.user
        events = list(Events.objects.filter(object=user))

        return render(request, EVENTS_TEMPLATE, {'events': events})


@method_decorator(login_required, name='dispatch')
class EventView(PasswordChangeRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        user = request.user
        event_id = kwargs.get('event_id')
        try:
            event = Events.objects.get(id=event_id)
        except Exception as e:
            return redirect(to='/')

        if event.object != user or event.subject != user:
            return redirect(to='/')
        event.details['seen'] = True
        event.save()

        return render(request, EVENT_VIEW_TEMPLATE, {'event': event})
