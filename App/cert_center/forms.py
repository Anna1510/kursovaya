from django import forms


class DocumentUploadForm(forms.Form):
    file = forms.FileField()


class PrivateKeyUploadForm(forms.Form):
    file = forms.FileField()
    CHOICES = [('yes', 'Да'), ('no', 'Нет')]
    agreed = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)
