from django.contrib.auth.views import redirect_to_login


class PasswordChangeRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        if request.user.edcuser.password_changed:
            return super().dispatch(request, *args, **kwargs)
        else:
            return redirect_to_login(request.get_full_path())
