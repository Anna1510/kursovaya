from django.urls import path
from .views import CertificationCenterView, CertificatesCreateView, \
    SendDocumentView, SignDocumentView, SignDocumentAgreementView, MyDocumentsView, ChooseReceiverView, \
    CertificatesView, CertificateView, RevokeCertificateView, CheckDocumentView, EventsView, EventView


urlpatterns = [
    path('', CertificationCenterView.as_view()),
    path('certificates/create/', CertificatesCreateView.as_view()),
    path('documents/send/<int:doc_id>', SendDocumentView.as_view()),
    path('documents/sign/<int:doc_id>', SignDocumentView.as_view()),
    path('documents/sign/<int:doc_id>/agree', SignDocumentAgreementView.as_view()),
    path('documents/', MyDocumentsView.as_view()),
    path('documents/<int:doc_id>/choose_receiver', ChooseReceiverView.as_view()),
    path('certificates/', CertificatesView.as_view()),
    path('certificates/current/', CertificateView.as_view()),
    path('certificates/<int:cert_id>/', CertificateView.as_view()),
    path('certificates/<int:cert_id>/revoke/', RevokeCertificateView.as_view()),
    path('documents/<int:doc_id>/check/', CheckDocumentView.as_view()),
    path('events/', EventsView.as_view()),
    path('events/<int:event_id>', EventView.as_view()),
]
