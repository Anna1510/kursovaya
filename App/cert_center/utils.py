from .models import Document, Certificate
from django.db.models import Model


class WrongDocException(Exception):
    pass


class WrongCertException(Exception):
    pass


def safe_get_document_by_id(doc_id, user):
    if not doc_id:
        raise WrongDocException('doc id is not provided')
    try:
        document = Document.objects.get(id=doc_id)
    except Model.DoesNotExist:
        raise WrongDocException('wrong doc id for user')
    except Model.MultipleObjectsReturned:
        raise WrongDocException(f'Multiple objects returned for {doc_id}, contact admin!!!')

    # проверяем принадлежит ли этот документ текущему пользователю
    if not document.check_if_belongs_to_user(user):
        raise WrongDocException('wrong doc id for user')

    return document


def safe_get_certificate_by_id(cert_id, user):
    if not cert_id:
        raise WrongCertException('doc id is not provided')
    try:
        certificate = Certificate.objects.get(id=cert_id)
    except Model.DoesNotExist:
        raise WrongCertException('wrong cert id for user')
    except Model.MultipleObjectsReturned:
        raise WrongCertException(f'Multiple objects returned for {cert_id}, contact admin!!!')

    # проверяем принадлежит ли этот сертификат текущему пользователю
    if not certificate.check_if_belongs_to_user(user):
        raise WrongCertException('wrong doc id for user')

    return certificate
