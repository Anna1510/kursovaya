from django.db import models
from django.contrib.auth.models import User, AnonymousUser
from .choices import EVENT_TYPE_CHOICES


class Certificate(models.Model):
    starting_date = models.DateTimeField()
    ending_date = models.DateTimeField()
    owner = models.ForeignKey(to=User, on_delete=models.CASCADE, null=True, blank=True)
    status = models.CharField(max_length=50)
    explanation = models.TextField(null=True, blank=True)
    serial = models.IntegerField(null=True)
    public_key_filename = models.CharField(max_length=150, null=True)

    def check_if_belongs_to_user(self, user):
        if isinstance(user, AnonymousUser):
            return False
        return len(Certificate.objects.filter(owner=user, id=self.id)) == 1


class Document(models.Model):
    from_who = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True, related_name='sender')
    to_who = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True, related_name='receiver')
    download_date = models.DateTimeField()
    hash = models.CharField(max_length=520)
    signature_date = models.DateTimeField(null=True)
    path = models.CharField(max_length=1024)
    certificate = models.ForeignKey(to=Certificate, on_delete=models.SET_NULL, null=True)
    original_name = models.CharField(max_length=100, blank='', default='')
    sent_date = models.DateTimeField(blank=True, null=True)
    checked = models.BooleanField(default=False)
    received_date = models.DateTimeField(null=True)
    signature_path = models.CharField(max_length=520, null=True, default=None)

    def check_if_belongs_to_user(self, user):
        if isinstance(user, AnonymousUser):
            return False
        return len(Document.objects.filter(from_who=user, id=self.id)) == 1


class Events(models.Model):
    date = models.DateTimeField()
    type = models.CharField(max_length=10, choices=EVENT_TYPE_CHOICES)
    subject = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True, related_name='subject')
    object = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True, related_name='object')
    details = models.JSONField(null=True, default=None)
