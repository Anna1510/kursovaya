from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth import authenticate, login, logout
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import ValidationError
import json
import datetime
from .forms import LoginForm, NewPasswordForm

TEMPLATES_SUB_FOLDER = 'basic_system/'
INITIAL_LOGIN_TEMPLATE = TEMPLATES_SUB_FOLDER + 'login.html'
AGREEMENT_TEMPLATE = TEMPLATES_SUB_FOLDER + 'agreement.html'
NEW_PASSWORD_TEMPLATE = TEMPLATES_SUB_FOLDER + 'new_password.html'
LOGOUT_TEMPLATE = TEMPLATES_SUB_FOLDER + 'logout.html'


class LoginView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect(to='/agreement/')
        form = LoginForm()
        return render(request, INITIAL_LOGIN_TEMPLATE, {'form': form})

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            clean_data = form.cleaned_data
            user = authenticate(request, username=clean_data['login'], password=clean_data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect(to='/agreement/')
                else:
                    return render(request, INITIAL_LOGIN_TEMPLATE, )
        return render(request, INITIAL_LOGIN_TEMPLATE, {'form': form, 'error': True})


class AgreementView(View):
    def get(self, request):
        user = request.user
        if user.edcuser.agreement_details:
            return redirect(to='/newpassword/')
        return render(request, AGREEMENT_TEMPLATE)

    def post(self, request):
        user = request.user
        if user.edcuser.agreement_details:
            return redirect(to='/newpassword/')
        if json.loads(request.body)['agreed']:
            details = {'agreement_time': datetime.datetime.now(),
                       'agreement_user_agent': request.META['HTTP_USER_AGENT']
                       }
            user.edcuser.agreement_details = json.dumps(details, cls=DjangoJSONEncoder)
            user.edcuser.save()
        return redirect(to='/newpassword/')


@method_decorator(login_required, name='dispatch')
class NewPasswordView(View):
    def get(self, request):
        user = request.user
        if user.edcuser.password_changed:
            return redirect('/')
        if not user.edcuser.agreement_details:
            return redirect(to='/agreement/')
        form = NewPasswordForm()
        return render(request, NEW_PASSWORD_TEMPLATE, {'form': form})

    def post(self, request):
        form = NewPasswordForm(request.POST)
        user = request.user
        if not user.edcuser.agreement_details:
            return redirect(to='/agreement/')
        if form.is_valid():
            try:
                new_password = form.check_password(user)
            except ValidationError as error:
                return render(request, NEW_PASSWORD_TEMPLATE, {'form': form, 'errors': error})

            user.edcuser.password_changed = True
            user.edcuser.save()

            user.set_password(new_password)
            user.save()

            login(request, user)
        else:
            return render(request, NEW_PASSWORD_TEMPLATE, {'form': form})
        return redirect(to='/')


@method_decorator(login_required, name='dispatch')
class LogoutView(View):
    def get(self, request):
        return render(request, LOGOUT_TEMPLATE)

    def post(self, request):
        logout(request)
        return redirect(to='/')
