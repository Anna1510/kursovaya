from django.urls import path
from .views import LoginView, AgreementView, NewPasswordView, LogoutView


urlpatterns = [
    path('signin/', LoginView.as_view()),
    path('agreement/', AgreementView.as_view()),
    path('newpassword/', NewPasswordView.as_view()),
    path('logout/', LogoutView.as_view())
]
