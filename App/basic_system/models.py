from django.db import models
from django.contrib.auth.models import User


class EDCUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    public_key = models.FileField(max_length=150, null=True, blank=True)
    middle_name = models.CharField(max_length=150, null=True)
    photo = models.ImageField()
    position = models.CharField(max_length=300)
    department = models.CharField(max_length=300)
    passport_series = models.CharField(max_length=6, null=False)
    passport_number = models.CharField(max_length=8, null=False)
    agreement_details = models.JSONField(null=True, blank=True)
    password_changed = models.BooleanField(default=False)

    def get_working_certificate(self):
        certificate = list(self.user.certificate_set.filter(status='Действует'))
        if certificate:
            return certificate[0]
        return None
